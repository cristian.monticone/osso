import requests
import bs4
import time
import sys

OPENHUB = "https://www.openhub.net"
project_URL = "/p/"
similar_URL = "/similar/"

PROJECT_FILE = "project.csv"
LICENSE_FILE = "license.csv"
FEATURE_FILE = "feature.csv"
ICON_FILE = "icon.csv"
LINK_FILE = "link.csv"
DEPS_FILE = "deps.csv"

icon_selector = ('#project_icon img')
name_selector = ('h1 a')
description_selector = ('#project_summary')
tag_selector = ('.tag')
link_selector = ('.quick_reference_container a')
license_selector = ('.license_title a')

separator = ","
EXPLORE_DEPTH = 1
visited = set()

def write_project_detail(soup):
    with open(PROJECT_FILE, 'a') as target:
        target.write("(")
        if(len(soup.select(name_selector)) > 0):
            target.write("'"+soup.select(name_selector)[0].getText()+"'"+separator)
        if(len(soup.select(description_selector)) > 0):
            target.write("'"+soup.select(description_selector)[0].getText()+"'")
        target.write("),\n")

def write_project_icon(soup):
    with open(ICON_FILE, 'a') as target:
        target.write("(")
        if(len(soup.select(name_selector)) > 0):
            target.write("'"+soup.select(name_selector)[0].getText()+"'"+separator)
        if(len(soup.select(icon_selector)) > 0):
            target.write("'"+soup.select(icon_selector)[0].attrs['src']+"'")
        target.write("),\n")

def write_project_feature(soup):
    with open(FEATURE_FILE, 'a') as target:
        for tag in soup.select(tag_selector):
            target.write("(")
            if(len(soup.select(name_selector)) > 0):
                target.write("'"+soup.select(name_selector)[0].getText()+"'"+separator)
            target.write("'"+tag.getText()+"'")
            target.write("),\n")

def write_project_link(soup):
    with open(LINK_FILE, 'a') as target:
        for link in soup.select(link_selector):
            target.write("(")
            if(len(soup.select(name_selector)) > 0):
                target.write("'"+soup.select(name_selector)[0].getText()+"'"+separator)
            target.write("'"+link.attrs['href']+"'")
            target.write("),\n")

def write_project_license(soup):
    with open(LICENSE_FILE, 'a') as target:
        for license in soup.select(license_selector):
            target.write("(")
            if(len(soup.select(name_selector)) > 0):
                target.write("'"+soup.select(name_selector)[0].getText()+"'"+separator)
            target.write("'"+license.getText()+"'")
            target.write("),\n")

def write_project_dep(project_name, project_soup):
    debian = "https://packages.debian.org/sid/"
    try:
        soup = bs4.BeautifulSoup(requests.get(debian+project_name).text, features='lxml')
    except:
        return False
    if (len(soup.select('.perror p')) <= 0 or 
        soup.select('.perror p')[0].getText() != 'No such package.'):
        for dep in soup.select("#pdeps dt"):
            dep = dep.getText().replace('\n','').replace('\t','')
            if dep.find('[') >= 0:
                dep = dep.split('[')[0].strip()
            with open(DEPS_FILE, 'a') as target:
                if (dep[0:2] != 'or' and len(project_soup.select(name_selector)) > 0
                        and dep.find(':') >= 0):
                    target.write("(")
                    target.write("'"+project_soup.select(name_selector)[0].getText()+"'"+separator)
                    target.write("'"+dep.split(':')[0]+"'"+separator)
                    if dep.find('(') >= 0 and dep.find(')') >= 0:
                        target.write("'"+dep.split(':')[1].split('(')[0].strip()+"'"+separator)
                        target.write("'"+dep.split('(')[1].split(')')[0].strip()+"'")
                    else:
                        target.write("'"+dep.split(':')[1].strip()+"'")
                    target.write("),\n")
    if len(soup.select("#pdeps dt")) > 0:
        return True

def explore(project_name, depth):
    if depth > EXPLORE_DEPTH:
        return
    resource = requests.get(OPENHUB+project_URL+project_name)
    soup = bs4.BeautifulSoup(resource.text, features='lxml')
    with open(PROJECT_FILE, 'r') as source:
        for line in source:
            if line.find("('"+soup.select(name_selector)[0].getText()+"'"+separator) > -1:
                print(project_name +" already recorded")
                return
    if not write_project_dep(project_name, soup):
        return
    write_project_detail(soup)
    write_project_icon(soup)
    write_project_feature(soup)
    write_project_link(soup)
    write_project_license(soup)
    visited.add(resource.url.split('/')[-1])
    print("Completed "+project_name)
    resource = requests.get(OPENHUB+project_URL+project_name+similar_URL)
    soup = bs4.BeautifulSoup(resource.text, features='lxml')
    for project in soup.select('#page_contents div:nth-child(2) h4 a'):
        url = project.attrs['href'].split('/')[-1]
        name = project.getText()
        if url in visited:
            print(url+' already visited')
        elif name == '(Compare Project)':
            url = project.attrs['href'].split('/')[-1]
        else:
            explore(project.attrs['href'].split('/')[-1], depth+1)

with open(PROJECT_FILE, 'a') as out:
    out.write('')
explore(sys.argv[1], 0)
