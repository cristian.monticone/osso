# Requisiti progetto

## Relazione

- [x] Motivazioni
- [x] Requirements
	- [x] Finalità
	- [x] Task specifici
	- [x] Tipologia utenti
- [x] Descrizione dominio
- [x] Documentazione
- [x] Uno tra
	- [x] [LODE](https://essepuntato.it/lode/)
	- [ ] [~~OOPS~~](http://oops.linkeddata.es/)
- [x] Visualizzazione ontologia
	- [x] Tassonomia classi
	- [x] Knowledge graph
	- [x] Triple dell'esempio della sezione 3 (non ho idea di cosa intenda)
- [x] Ontologia commentata in formato Turtle

## Esercizio modellazione

- [x] 16 classi
- [x] 8 object properties
- [x] 4 data properties
- [x] tassonomia a 4 livelli
- [x] Relazioni inverse
- [x] Relazioni transitive
- [x] Relazioni funzionali
- [x] 4 classi definite con restrizioni a scelta

Riporto il testo delle specifiche perché non so se la mia interpretazione sia corretta:
>almeno 2 classi definite con restrizioni some e min (4 restrizioni a scelta per gruppi di due).

- [x] Classe enumerata
- [x] Pattern (più di uno?)
- [x] 2 property chain
- [x] A-Box con 4 esempi completi
- [x] Allineamento con 2 con ontologie standard tramite SKOS/rdfs:subclass/owl:equivalentClass con motivazione.
  - [x] Allineamento con spdx.org per le licenze
  - [x] Allineamento con wikidata per linguaggio di programmazione e autore.
- [x] Il ragionamento deve aggiungere asserzioni di classi e proprietà.
- [x] Inferenze materializzate

## SPARQL

- [x] Flow chart interazione
- [x] Schema interfaccia (mockup senza grafica)
- [x] Esempio interazione con dati reali
- [x] 8 query, per ognuna:
	- [x] query
	- [x] descrizione
	- [x] risultato su dati d'esempio
- [x] 2 query che interrogano endpoint SPARQL pubblico per integrazione dati nell'ontologia.

## Estensioni

- [x] Applicazione client
	- [x] Interrogazione ontologia tramite SPARQL.
	- [x] Query su altri sorgenti di dati
	- [x] Codice commentato
	- [x] Documentazione (1/2 pagina)
- [x] Regole SWRL (5/5)
