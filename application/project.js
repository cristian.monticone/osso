// Virtuoso SPARQL js module
import {sparql_query, BASE_PREFIXES} from './sparql.js';

// Default software project
const DEFAULT_SOFTWARE = 'Vim';

// Main routine
$(document).ready(function(){
	// Retrieving software project from URL parameters
	var searchParams = new URLSearchParams(window.location.search);
	var project = (searchParams.has('project')) ? searchParams.get('project') : DEFAULT_SOFTWARE;

	// Retrieving software informations.
	get_project_info(project); // Retrieving project main info.
	get_project_features(project); // Retrieving features.
	get_project_licenses(project); // Retrieving licenses.
	get_project_OSs(project); // Retrieving OSs.
	get_project_authors(project); // Retrieving authors.
	get_programming_languages(project); // Retrieving programming languages.
	get_project_similar_apps(project); // Retrieving alternative and similar applications.
	get_project_dependencies(project); // Retrieving dependencies.
	get_project_abstract_projects(project); // Retrieving all abstract software the project implements.
	get_project_implementations(project); // Retrieving all implementations of an abstract software.
});

// Project main informations.
function get_project_info(project) {
	// SPARQL query.
	var query = 
BASE_PREFIXES + `
SELECT ?p_name ?p_desc ?gallery_list ?initialRelease ?homepage ?repository ?wiki ?forum
WHERE {
  :` + project + ` 
        :projectName ?p_name;
        :projectDescription ?p_desc.
    OPTIONAL { :` + project + ` :projectImageList ?gallery_list}.
    OPTIONAL { :` + project + ` :initialRelease ?initialRelease}.
    OPTIONAL { :` + project + ` :projectHomepage ?homepage}.
    OPTIONAL { :` + project + ` :projectRepository ?repository}.
    OPTIONAL { :` + project + ` :projectWiki ?wiki}.
    OPTIONAL { :` + project + ` :projectForum ?forum}.

 }
	`;

	// Perform query.
	sparql_query(query, (result) => {
		var data = result.results.bindings[0];

		if (data != undefined) { // Project exists.
			$('#project_name').html(data.p_name.value); // Set name into DOM
			$('#project_description').html(data.p_desc.value); // Set description into DOM

			// If a gallery is defined: handle it. Hide gallery container otherwise.
			if(data.gallery_list != undefined) 
				get_project_gallery(project);
			else
				$('#gallery-col').hide();

			if(data.homepage != undefined) // If homepage defined: set it.
				$('#support_sites').append('<a href="' + data.homepage.value + '">Homepage</a> ');
			if(data.repository != undefined) // If codebase repo defined: set it.
				$('#support_sites').append('<a href="' + data.repository.value + '">Repository</a> ');
			if(data.wiki != undefined) // If wiki defined: set it.
				$('#support_sites').append('<a href="' + data.wiki.value + '">Wiki</a> ');
			if(data.forum != undefined) // If forum defined: set it.
				$('#support_sites').append('<a href="' + data.forum.value + '">Forum</a> ');
		}
		else { // Project doesn't exist!
			console.error("Wrong project indentifier! Project not found...");
		}
	});
}

// Abstract projects that are implemented by 'project'
function get_project_abstract_projects(project) {
	// Query
	var query =
BASE_PREFIXES + `

SELECT DISTINCT ?abstractProject ?abstractProjectName
WHERE {
    :` + project + ` :implementationOf ?abstractProject.
    ?abstractProject :projectName ?abstractProjectName.
 }
	`;

	// Perform query
	sparql_query(query, (result) => {
		if (result.results.bindings.length === 0) $('#implementationOf_row').hide(); // No abstract software implementations
		else
			jQuery.each(result.results.bindings, (i, val) => { // for each abstract software in result.
				$('#implementationOf').append( // Add abstract software ref.
					'<a href="?project=' +
					val.abstractProject.value.split('#')[1] + '">' +
					val.abstractProjectName.value +
					'</a> '
					);
			});
	});
}

// Implementations of an abstract software
function get_project_implementations(project) {
	// Query
	var query =
BASE_PREFIXES + `

SELECT DISTINCT ?implementedProject ?implementedProjectName
WHERE {
    :` + project + ` :implementedBy ?implementedProject.
    ?implementedProject :projectName ?implementedProjectName.
 }

	`;

	// Perform query
	sparql_query(query, (result) => {
		if (result.results.bindings.length === 0) $('#implementedBy_row').hide(); // No implemented softwares
		else
			jQuery.each(result.results.bindings, (i, val) => { // for each implementation software in result.
				$('#implementedBy').append( // Add implementation software ref.
					'<a href="?project=' +
					val.implementedProject.value.split('#')[1] + '">' +
					val.implementedProjectName.value +
					'</a> '
					);
			});
	});
}

// Project features.
function get_project_features(project) {
	// SPARQL query
	var query =
BASE_PREFIXES + `
SELECT ?f_name ?f_desc
WHERE {
   :` + project + ` :provideFeature ?feature.
    ?feature :featureName ?f_name.
    OPTIONAL { ?feature :featureDescription ?f_desc}.
 }
	`;

	// Perform query
	sparql_query(query, (result) => {
		if (result.results.bindings.length === 0) $('#feature_row').hide(); // No features for this project...
		else
			jQuery.each(result.results.bindings, (i, val) => { // for each feature in result.
				$('#feature_list').append( // Add feature badge.
					'<span class="badge rounded-pill bg-primary" title="' +
					((val.f_desc) ? val.f_desc.value : "A software feature.") + '">' +
					val.f_name.value +
					'</span> '
					);
			});
	});
}

// OSs supported by project.
function get_project_OSs(project) {
	// Query
	var query =
BASE_PREFIXES + `
SELECT DISTINCT ?os_name ?os_desc ?homepage
WHERE {
    :` + project + `(:isSupportedBy | :isDistributedBy) ?os.
    ?os rdfs:label ?os_name.
    ?os rdfs:comment ?os_desc.
    OPTIONAL {?os foaf:homepage ?homepage}.
 }
	`;

	// Perform query
	sparql_query(query, (result) => {
		if (result.results.bindings.length === 0) $('#supportedOSs_row').hide(); // No supported OSs
		jQuery.each(result.results.bindings, (i, val) => { // for each OS in result.
			$('#OSs').append( // Add OS link
				'<a href="' +
				((val.homepage) ? val.homepage.value : "#") + 
				'" title="' + val.os_desc.value +'">' +
				val.os_name.value +
				'</a> '
				);
		});
	});
}

const MAX_SIMILAR_APP = 3; // Max suggested similar apps.
const MIN_IN_COMMON_FEATURES = 2; // Min feature in common between two apps.
// Similar application
function get_project_similar_apps(project) {
	// Query
	var query =
BASE_PREFIXES + `
SELECT ?p (COUNT(?p) AS ?similarity)
WHERE {
    {SELECT DISTINCT ?p WHERE {?p a/rdfs:subClassOf* :Software. FILTER (?p != :` + project + `)}}
    ?p :provideFeature/^:provideFeature :` + project + `
 }
GROUP BY ?p
HAVING (COUNT(?p) >= ` + MIN_IN_COMMON_FEATURES + `)
ORDER BY DESC (?similarity)
LIMIT ` + MAX_SIMILAR_APP + `
	`;
	
	// Perform query
	sparql_query(query, (result) => {
		if (result.results.bindings.length === 0) $('#sim_app_row').hide(); // No similar apps for this project.
		else jQuery.each(result.results.bindings, (i, val) => { // for each OS in result.
			$('#similar_apps').append( // Add similar app link
				'<a href="project.html?project=' +
				val.p.value.split('#')[1] + '">' +
				val.p.value.split('#')[1] +
				'</a> '
				);
		});
	});
}

// Project licenses
function get_project_licenses(project) {
	// Query
	var query =
BASE_PREFIXES + `
PREFIX spdx: <http://spdx.org/rdf/terms#>

SELECT ?license ?name
FROM <http://spdx.org/rdf/terms#>
WHERE {
    :` + project + ` :isReleasedUnder/owl:sameAs? ?license.
    ?license spdx:name ?name
 }
 GROUP BY ?license
	`;

	// Perform query
	sparql_query(query, (result) => {
		jQuery.each(result.results.bindings, (i, val) => { // for each license in result.
			if (! val.license.value.includes("purl.org/osso")) // Not include internal osso license copies.
				$('#licenses').append( // Add license link
					'<a href="' + val.license.value + '" style="margin-left:10px" class="badge badge-pill bg-success">' +
					val.name.value + '</a>'
					);
			});
	});
}

// Project authors
function get_project_authors(project) {
	// Federated query
	var query =
BASE_PREFIXES + `
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>


SELECT DISTINCT ?author SAMPLE(?author_label) as ?label
WHERE {
    :` + project + ` skos:relatedMatch ?wd_item
    SERVICE <https://query.wikidata.org/bigdata/namespace/wdq/sparql>
        {
            ?wd_item (wdt:P170|wdt:P178) ?author. # P170: creator; P178: developer;
            ?author rdfs:label ?author_label.
            filter langMatches(lang(?author_label), "en") }
}
GROUP BY ?author
	`;

	// Perform query
	sparql_query(query, (result) => {
		if (result.results.bindings.length === 0) $('#authors_row').hide(); // No authors found
		else jQuery.each(result.results.bindings, (i, val) => { // for each author/dev in result.
			$('#authors').append(
				'<a href="' + val.author.value + '">' +
				val.label.value + '</a> '
				);
		});
	}, "");
}

// Get programming language
function get_programming_languages(project) {
	// Federated query
	var query =
BASE_PREFIXES + `
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT DISTINCT ?language SAMPLE(?language_label) as ?label
WHERE {
    :` + project + ` skos:relatedMatch ?wd_item
    SERVICE <https://query.wikidata.org/bigdata/namespace/wdq/sparql>
        {
            ?wd_item wdt:P277 ?language. # P277: programming language
            ?language rdfs:label ?language_label.
            filter langMatches(lang(?language_label), "en") }
            
}
GROUP BY ?language
	`;

	// Perform query
	sparql_query(query, (result) => {
		if (result.results.bindings.length === 0) $('#languages_row').hide(); // No languages found
		jQuery.each(result.results.bindings, (i, val) => { // for each language in result.
			$('#languages').append(
				'<a href="' + val.language.value + '">' +
				val.label.value + '</a> '
				);
		});
	}, "");
}

// Project gallery
function get_project_gallery(project) {
	// Query
	var query =
BASE_PREFIXES + `
PREFIX c_entity: <http://www.ontologydesignpatterns.org/cp/owl/collectionentity.owl#>
PREFIX bag: <http://www.ontologydesignpatterns.org/cp/owl/bag.owl#>

SELECT ?imageIRI
WHERE {
    :` + project + ` :projectImageList/c_entity:hasMember/bag:itemContent/:imageURI ?imageIRI
 }
	`;

	// Perform query
	sparql_query(query, (result) => {
		jQuery.each(result.results.bindings, (i, val) => { // for each image IRI in result.
			$('#gallery').append('<div class="carousel-item '  +  // Add image into carousel
					     ((i===0)? "active" : "") + 
					     '"><img src="' + val.imageIRI.value +
					     '" class="d-block w-100"></div>');
		});
	});
}

// Project dependencies
function get_project_dependencies(project) {
	// Query
	var query =
BASE_PREFIXES + `
SELECT ?dep_target ?p_name ?minVersion ?exactVersion ?required ?runtime ?buildtime ?isAbstract ?isAlternative ?altTarget
WHERE {
    :` + project + ` :hasDependency ?dep.
    ?dep :dependencyTarget ?dep_target.
    ?dep_target :projectName ?p_name.
    OPTIONAL {?dep :minVersion ?minVersion}.
    OPTIONAL {?dep :exactVersion ?exactVersion}.
    OPTIONAL {?dep :isRequired ?required}.
    OPTIONAL {?dep :runtimeDependency ?runtime}.
    OPTIONAL {?dep :buildtimeDependency ?buildtime}.
    BIND (EXISTS{?dep_target a :AbstractSoftware} AS ?isAbstract).
    BIND (EXISTS{?dep :alternativeDependency []} AS ?isAlternative).
    OPTIONAL {?dep :alternativeDependency ?altDep.
    	      ?altDep :dependencyTarget ?altTarget. FILTER (?dep != ?altDep)}
 }
	`;

	// Perform query
	sparql_query(query, (result) => {
		if (result.results.bindings.length === 0) // No deps
			$('#dependencies_table ').html("No registered dependencies for this software.");
		jQuery.each(result.results.bindings, (i, val) => { // for each dependency in result.
			var fragment_identifier = val.dep_target.value.split('#')[1];

			var required = "";
			if (val.required) {// is isRequired defined?
				required = (val.required.value == "1") ? "required" : "optional";
			}

			// Compose release constraint
			var version = (val.minVersion) ? ">=" + val.minVersion.value : "";
			version = (val.exactVersion) ? "= " + val.exactVersion.value : version;

			var other_info = "";
			if (val.runtime) { // is runtime defined?
				other_info += (val.runtime.value == "1") ? "runtime; " : "";
			}
			if (val.buildtime) { // is buildtime defined?
				other_info += (val.buildtime.value == "1") ? "buildtime; " : "";
			}

			// Specific dependency informations
			other_info += (val.isAbstract.value == "1") ? "abstract; " : ""; // Abstract dep.

			other_info += (val.isAlternative.value == "1") ? "alternative(" + // Alternative dep.
				val.altTarget.value.split('#')[1] + "); " : "";

			// Dependencie entry row
			$('#dependencies_table > tbody').append(
				'<tr>' +
				'<tr><th scope="row">' + (i+1) + '</th>' +
				'<td><a href="project.html?project=' + fragment_identifier + '">' + val.p_name.value + '</a></td>' +
				'<td>' + required + '</td>'+
				'<td>' + version + '</td>'+
				'<td>' + other_info + '</td>' +
				'</tr>'
				);
		});
	});
}
