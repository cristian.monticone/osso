// Virtuoso SPARQL js module
import {sparql_query, BASE_PREFIXES} from './sparql.js';

// OS, feature and license filter Set data structures
var os_filter_set = new Set();
var feature_filter_set = new Set();
var license_filter_set = new Set();

// Main routine
$(document).ready(function(){
	$('#search_button').click(() => {search()}); // Search button trigger.
	$('#search_field').keyup((e) => { // Enter key trigger on search field.
		if(e.keyCode == 13) search();
	});

	// Set filter menu entries.
	set_os_filter();
	set_feature_filter();
	set_license_filter();
});

// This function performs a filtered search over OSSO software projects.
function search() {
	var os_filters = "";
	var feature_filters = "";
	var license_filters = "";

	// Converting OS filters defined by user into sparql language.
	os_filter_set.forEach((os) => {
		os_filters += "?p (:isSupportedBy | :isDistributedBy) :" + os + ".";
	});
	// Converting feature filters defined by user into sparql language.
	feature_filter_set.forEach((feature) => {
		feature_filters += "?p :provideFeature :" + feature + ".";
	});
	// Converting license class filters defined by user into sparql language.
	license_filter_set.forEach((licenseClass) => {
		feature_filters += "?p :isReleasedUnder/a/rdfs:subClassOf* :" + licenseClass + ".";
	});
	
	// SPARQL query
	var query = 
BASE_PREFIXES +`
SELECT DISTINCT ?p_name ?p
WHERE {
    ?p :projectName ?p_name.
    FILTER regex(?p_name, "` + $('#search_field').val()  + `", "i").
    ` + os_filters + `
    ` + feature_filters + `
}
	`;

	$('#results').empty(); // Remove old results.
	$('#result_container').show(); // Showing results container.
	$('#search_spinner').show(); // Spinning icon while searching.

	// Performing query
	sparql_query(query, (result) => {
		$('#search_spinner').hide(); // Hiding spinner

		// No software founds.
		if (result.results.bindings.length === 0) {
			console.warn("No software founds.");
			$('#results').html("No matches... retry with other filters.");
		}
		else {
			jQuery.each(result.results.bindings, (i, val) => { // for each project in result.
				$('#results').append('<a href="project.html?project=' +
							val.p.value.split('#')[1] + '">' +  // select only fragment indentifier.
							val.p_name.value + '<br>'); // project name
			});
		}
	});
}

// Set os filter menu entries.
function set_os_filter() {
	// Query: retrieve all OS described in OSSO
	var query =
BASE_PREFIXES + `
SELECT DISTINCT ?os ?os_label
WHERE {
    ?os a/rdfs:subClassOf* :Operating_System.
    ?os rdfs:label ?os_label
 }
	`;

	// Perform query
	sparql_query(query, (result) => {
		jQuery.each(result.results.bindings, (i, val) => { // for each license in result.
			var os_identifier = val.os.value.split('#')[1]; // OS fragment identifier

			$('#os_filter_items').append( // Append OS html item into menu
					'<li><a id="' + os_identifier + '_os_adder"' +
					' class="dropdown-item" href="#">' + val.os_label.value + '</a></li>'
				);

			// Add OS click event handler
			$('#' + os_identifier + '_os_adder').click(() => {
				add_os_filter(os_identifier)
			});
		});
	});
}

// Set feature filter menu entries.
function set_feature_filter() {
	// Query: retrieve all feature described in OSSO
	var query =
BASE_PREFIXES + `
SELECT DISTINCT ?f ?f_name
WHERE {
    ?f a/rdfs:subClassOf* :Feature.
    ?f :featureName ?f_name
 }
	`;

	// Perform query
	sparql_query(query, (result) => {
		jQuery.each(result.results.bindings, (i, val) => { // for each license in result.
			var feature_identifier = val.f.value.split('#')[1]; // Feature fragment identifier

			$('#feature_filter_items').append( // Append feature html item into menu
					'<li><a id="' + feature_identifier + '_feature_adder' +
					'" class="dropdown-item" href="#">' + val.f_name.value + '</a></li>'
				);


			// Add feature click event handler
			$('#' + feature_identifier + "_feature_adder").click(() => {
				add_feature_filter(feature_identifier)
			});
		});
	});
}

// Set license filter menu entries.
function set_license_filter() {
	// Fixed license classes
	var licenses = ['Copyleft_license', 'FreeLicense', 'Non_commercial_license',
		'Permissive_license', 'Proprietary_license' ,'Public_domain_license'];

	// Add license click event handler
	licenses.forEach((license) => {
		$('#' + license + '_adder').click(() => {add_license_filter(license)});
	});
}

// Add license search filter
function add_license_filter(license) {
	if (!license_filter_set.has(license)) { // Filter not already setted: adding it.
		license_filter_set.add(license); // Add filter to Set data structure.

		$('#enabled_filters').append( // Adding visual filter into DOM
			'<div class="pr-4" id="' + license + '_filter" ><button id="' + license + '_remover" type="button" class="btn-close" aria-label="Delete filter"></button>' +
			'Released under "' + license + '"</div>'
		);

		// Handling remove filter events.
		$('#' + license + '_remover').click(() => {
			remove_license_filter(license);
		});
	}
	else { // Filter already setted: nothing to do...
		console.warn("License filter \"" + license + "\" already enabled!");
	}
}

// Add OS search filer
function add_os_filter(os) {
	if (!os_filter_set.has(os)) { // Filter not already setted: adding it.
		os_filter_set.add(os); // Add filter to Set data structure.

		$('#enabled_filters').append( // Adding visual filter into DOM
			'<div class="pr-4" id="' + os + '_os_filter" ><button id="' + os + '_os_remover" type="button" class="btn-close" aria-label="Delete filter"></button>' +
			'Available on "' + os + '"</div>'
		);

		// Handling remove filter events.
		$('#' + os + '_os_remover').click(() => {
			remove_os_filter(os);
		});
	}
	else { // Filter already setted: nothing to do...
		console.warn("OS filter \"" + os + "\" already enabled!");
	}
}

// Add feature search filter
function add_feature_filter(feature) {
	if (!feature_filter_set.has(feature)) { // Filter not already setted: adding it.
		feature_filter_set.add(feature); // Add filter to Set data structure.

		$('#enabled_filters').append( // Adding visual filter into DOM
			'<div class="pr-4" id="' + feature + '_feature_filter" ><button id="' + feature + '_feature_remover" type="button" class="btn-close" aria-label="Delete filter"></button>' +
			'Provide feature "' + feature + '"</div>'
		);

		// Handling remove filter events.
		$('#' + feature + '_feature_remover').click(() => {
			remove_feature_filter(feature);
		});
	}
	else { // Filter already setted: nothing to do...
		console.warn("Feature filter \"" + feature + "\" already enabled!");
	}
}

// Remove OS search filter
function remove_os_filter(os) {
	os_filter_set.delete(os);
	$('#' + os + "_os_filter").remove();
}

// Remove feature search filter
function remove_feature_filter(feature) {
	feature_filter_set.delete(feature);
	$('#' + feature + "_feature_filter").remove();
}

// Remove license search filter
function remove_license_filter(license) {
	license_filter_set.delete(license);
	$('#' + license + "_filter").remove();
}
