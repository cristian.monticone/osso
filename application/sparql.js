// SPARQL js module

// Standard default prefixes
export const BASE_PREFIXES = `
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
`;

// Default virtuoso sparql endpoint
const ENDPOINT = "http://purl.org/osso/sparql";

// Default graph URI
const GRAPH_URI = "http://purl.org/osso#";


/*
 * Perform a sparql query on a virtuoso sparql endpoint.
 *
 * query: A sparql query.
 * success_callback: Callback that handles the result in case of success.
 * graphUri: Graph URI (Could be omitted).
 * endpoint: Endpoint URL.
 * failure_callback: Failure ajax callback.
 *
 */
export function sparql_query(query, success_callback, graphUri=GRAPH_URI, endpoint=ENDPOINT, failure_callback=default_failure) {
	$.ajax({
		url: endpoint + 
			"?default-graph-uri=" + encodeURIComponent(graphUri) +
			"&query=" + encodeURIComponent(query) +
			"&format=application%2Fsparql-results%2Bjson&timeout=0&debug=on&run=+Run+Query+",
		method: 'GET',
		dataType: 'json',
		success: (result) => {success_callback(result)},
		error: (jqXHR, textStatus) => {failure_callback(textStatus)}
	});
}

// Default fail callback for sparql AJAX requests
function default_failure(textStatus) {
	console.error(textStatus);
}
