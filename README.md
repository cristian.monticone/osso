# Documentazione OSSO

La **Open Source Software Ontology** è un'ontologia finalizzata alla descrizione dei software, le loro licenze e i sistemi operativi sui quali essi sono distribuiti.

## Motivazioni

Il dominio dei software open source è stato scelto vista la sua rilevanza, acquisita in diversi anni, nello sviluppo software sia a livello amatoriale che professionale e dell'utilizzo personale come alternative più rispettose della privacy degli utenti.

Nell'ambito dello sviluppo molte librerie software open source sono impiegate in egual misura da software proprietari che da programmi open source.
Per verificarlo è sufficiente analizzare l'elenco delle licenze presenti in applicativi quali: 
+ Whatsapp, 8 licenze
+ Mozilla Firefox, 123 licenze
+ VLC, 23 licenze

Per quanto riguarda l'utilizzo personale, negli ultimi anni si è sviluppata una maggiore conoscenza su argomenti quali: la privacy, marketing mirato e sorveglianza di massa. 

Casi recenti come la modifica dei termini di Whatsapp e la crescita di utenza di Signal e Telegram dimostrano che se presentati ad alternative valide gli utenti scelgono il software ritenuto meno invasivo.

## Requirements

### Finalità e task

Vista la natura più **'libera'** e **spontanea** con cui nascono i progetti open source uno dei problemi che pone una importante limitazione alla sua diffusione è la __discoverability__.

Lo sviluppo dell'ontologia OSSO è finalizzato alla consultazione delle informazioni dei prodotti software e alla esplorazione della sua base di conoscenza alla ricerca di nuovi software.

La __discoverability__ si concretizza in due sfide più specifiche in base al tipo di utenza a cui si fa riferimento.

### Utilizzatori di software

Per questa tipologia di utente le informazioni prioritarie per valutare l'utilizzo di un software rispetto ad un altro sono:
+ le __feature__ che il prodotto offre.
+ i __sistemi operativi__ con cui il prodotto è compatibile.


### Programmatori

Per questi utenti oltre alle informazioni sopracitate è necessario conoscere: 
+ la __licenza__ con la quale il prodotto sia distribuito per assicurarsi che esso sia compatibile con licenza che s'intende porre al proprio progetto.
+ le __dipendenze__ che devono essere soddisfatte per poter utilizzare il prodotto.

## Descrizione del dominio

Il dominio che si è scelto di descrivere è quello dei software open source

Per __software__ intendiamo sia applicativi completi quali browser, editor di testo, riproduttori multimediali e altri che forniscono funzionalità all'utente finale, sia librerie e middleware che rendono disponibili strumenti e API utilizzati da altri software. Per i software inseriti all'interno dell'ontologia si sono usate come fonti le loro pagine su [GitHub](https://github.com), [GitLab](https://gitlab.com) e i relativi siti ufficiali.
Con software open source indichiamo quei progetti software che rendono disponibile il proprio codice sorgente gratuitamente con la possibilità di modificarlo e ridistribuirlo.

Proprio per queste due operazioni sono importanti le __licenze__ ovvero i documenti legali che descrivono quali attività sono permesse con il software.
Più specificamente sono le licenze free che rivestono un ruolo fondamentale per i software open source infatti esse offrono diversi livelli di garanzia affinché il codice sorgente possa essere: eseguito, studiato, modificato e ridistribuito.
Per la descrizione delle licenze si è usato come riferimento: [Software Package Data Exchange](https://spdx.org/rdf/terms/) e [GNU license list](https://www.gnu.org/licenses/license-list.html#SoftwareLicenses). 

I software possono richiedere che alcuni requisiti vengano soddisfatti affinché possano svolgere correttamente il proprio compito, questi vincoli vengono detti __dipendenze__ e si riferiscono ad altri software e, opzionalmente, a versioni specifiche di questi.

Un programma si dice __supportato__ da un __sistema operativo__ quando può essere eseguito da quest'ultimo, un programma si dice __distributo__ da un sistema operativo quando l'organizzazione che fornisce il sistema fornisce versioni modificate e preconfigurate per il proprio OS.

Infine si definisce come __feature__ la capacità di un software di fornire specifiche funzionalità all'utente o ad altri software.

## Documentazione

Data la grande varietà dei software open source e le differenti metodologie con cui essi vengono distribuiti non esistono standard universalmente applicati nella documentazione dei progetti. Per quanto non siano presenti questi standard, si sono formate col tempo alcune pratiche nella gestione dei progetti open source ampiamente diffuse che ci hanno permesso di documentare il dominio dell'ontologia OSSO.

### Internet hosting di repository git

Una delle pratiche più comuni all'interno dell'open source software development è la distribuzione del codice online tramite differenti soluzioni disponibili online.
Le piattaforme più popolari che offrono questi servizi sono [GitHub](https://github.com), [GitLab](https://gitlab.com), [SourceForge](https://sourceforge.net/). Esistono inoltre piattaforme più specifiche come ad esempio [PyPI](https://pypi.org/) per la distribuzione di moduli in linguaggio Python.

Su queste piattaforme per ogni progetto è molto comune trovare file scritti in MarkDown che forniscono una descrizione del progetto, ad esempio per il progetto Xournal++ questo è un estratto dal file README.md:

>## Features
>
>Xournal++ is a hand note taking software written in C++ with the target of flexibility, functionality and speed.
>Stroke recognizer and other parts are based on Xournal Code, which you can find at [sourceforge](http://sourceforge.net/projects/xournal/)
>
>Xournal++ features:
>
>- Support for pen pressure, e.g. Wacom Tablet
>- Support for annotating PDFs
>- [...]
>
>## Installing
>
>The official releases of Xournal++ can be found on the
>[Releases](https://github.com/xournalpp/xournalpp/releases) page. We provide
>binaries for Debian (Buster), Ubuntu (16.04), MacOS (10.13 and newer), and
>Windows. For other Linux distributions (or older/newer ones), we also provide an
>AppImage that is binary compatible with any distribution released around or
>after Ubuntu 16.04. For installing Xournal++ Mobile on handheld devices, please check out [Xournal++ Mobile's instructions](https://gitlab.com/TheOneWithTheBraid/xournalpp_mobile#try-it-out)
>
>**A note for Ubuntu/Debian users**: The official binaries that we provide are
>only compatible with the _specific version of Debian or Ubuntu_ indicated by the
>file name. For example, if you are on Ubuntu 20.04, the binary whose name
>contains `Ubuntu-bionic` is _only_ compatible with Ubuntu 18.04. If your system
>is not one of the specific Debian or Ubuntu versions that are supported by the
>official binaries, we recommend you use either the PPA (Ubuntu only), the Flatpak, or the
>AppImage.
>
>There is also an _unstable_, [automated nightly
>release](https://github.com/xournalpp/xournalpp/releases/tag/nightly) that
>includes the very latest features and bug fixes.
>
>With the help of the community, Xournal++ is also available on official repositories
>of some popular Linux distros and platforms.

Utilizzando queste informazioni siamo riusciti ad ottenere la descrizione, le feature del progetto e i sistemi operativi supportati dal software.
Inoltre è molto comune che nella repository di ogni progetto venga inserito un file contenente il testo della licenza sotto la quale si è scelto di rilasciare il software e un file che descriva le librerie e software richiesti per compilare il software in modo autonomo.

Il testo della licenza impiegata per Xournal++ (GPL-2.0) si trova al seguente [link](https://raw.githubusercontent.com/xournalpp/xournalpp/master/LICENSE).

Queste sono le dipendenze e i comandi necessari per compilare Xournal++ per Linux:
>### Distribution specific commands
>
>#### For Arch
>```bash
>sudo pacman -S cmake gtk3 base-devel libxml2 cppunit portaudio libsndfile \
>poppler-glib texlive-bin texlive-pictures gettext libzip
>```
>
>#### For Ubuntu/Debian:
>````bash
>sudo apt-get install cmake libgtk-3-dev libpoppler-glib-dev portaudio19-dev \
>	libsndfile-dev libcppunit-dev dvipng texlive libxml2-dev liblua5.3-dev \
>	libzip-dev librsvg2-dev gettext
>````
>[...]
>## Compiling
>
>The basic steps to compile Xournal++ are:
>
>```bash
>git clone http://github.com/xournalpp/xournalpp.git
>cd xournalpp
>mkdir build
>cd build
>cmake ..
>cmake --build .
># For a faster build, set the flag -DCMAKE_BUILD_TYPE=RelWithDebInfo
>```

### Software Package Data Exchange

Per quanto concerne la descrizione delle licenze abbiamo utilizzato i dataset e l'ontologia fornita da SPDX disponibile al seguente link [https://spdx.org/rdf/terms/](https://spdx.org/rdf/terms/).

I dataset utilizzati sono disponibili nella repository raggiungibile tramite [https://github.com/spdx/license-list-data](https://github.com/spdx/license-list-data).
Questo è un estratto della licenza GPL-2.0 utilizzata da Xournal++:
```
@prefix spdx:  <http://spdx.org/rdf/terms#> .
@prefix doap:  <http://usefulinc.com/ns/doap#> .
@prefix rdf:   <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix rdfs:  <http://www.w3.org/2000/01/rdf-schema#> .

<http://spdx.org/licenses/GPL-2.0-only>
        a                             spdx:License ;
        spdx:isFsfLibre               "true" ;
        spdx:isOsiApproved            "true" ;
        spdx:licenseId                "GPL-2.0-only" ;
        spdx:name                     "GNU General Public License v2.0 only" ;
```

### Wikipedia

Per i progetti software dei quali esiste una pagina Wikipedia sono state usate alcune delle informazioni presenti nell'infobox riassuntivo.

Ad esempio per l'editor di testo Vim abbiamo ricavato l'informazione sulla prima release come si può vedere dall'immagine:

![Vim Wikipedia infobox](./doc/vim_infobox.png)

### Package manager e distributor

Un altra fonte di informazioni sul dominio dei software open source sono stati i dati forniti dai differenti packet manager e dai portali web che riportano le informazioni.

Ad esempio utilizzando l'Advanced Package Tool per Xournal++ con il seguente comando:
>$ apt show xournalpp

Si ottiene il seguente output:
```
Package: xournalpp
Version: 1.0.20-hotfix~git20201227.1623-nogitfound-buster-1
Status: install ok installed
Priority: optional
Section: graphics
Maintainer: Andreas Butti <andreasbutti@gmail.com>
Installed-Size: 4.558 kB
Depends: libglib2.0-0 (>= 2.32), libgtk-3-0 (>= 3.18), libpoppler-glib8 (>= 0.41.0),
	libxml2 (>= 2.0.0), libportaudiocpp0 (>= 12), libsndfile1 (>= 1.0.25),
	liblua5.3-0, libzip4 (>= 1.0.1) libzip5, zlib1g, libc6
Suggests: texlive-base, texlive-latex-extra
Download-Size: sconosciuto
APT-Manual-Installed: yes
APT-Sources: /var/lib/dpkg/status
Description: Xournal++ - Open source hand note-taking program
 Xournal++ is a hand note taking software written in C++ with the target of
 flexibility, functionality and speed. Stroke recognizer and other parts are
 based on Xournal Code, which you can find at sourceforge.
 It supports Linux (e.g. Ubuntu, Debian, Arch, SUSE), macOS and Windows 10.
 Supports pen input from devices such as Wacom Tablets.
 .
 Xournal++ features:
 .
  - Support for Pen pressure, e.g. Wacom Tablet
  - Support for annotating PDFs
  [...]
```

E sempre dello stesso progetto queste sono le informazioni disponibili tramite [archlinux.org](https://archlinux.org/packages/community/x86_64/xournalpp/):

![Xournal++ Arch](./doc/xournalpp_arch.png)

Bisogna specificare che le informazioni fornite dai packet manager sono specifiche per il sistema operativo per il quale stanno operando quindi alcune informazioni, come le dipendenze, non sono state direttamente inserite ma sono state valutate per singolo progetto software.

Altri portali web crowdsourced come [OpenHub](https://www.openhub.net) e [AlternativeTo](https://alternativeto.net/) sono stati utilizzati per verificare la correttezza dei dati inseriti.

## Visualizzazione

### Tassonomia

Classi top level, le classi in __rosso__ sono state importate da ontologie esterne:

![Top Level](doc/visualization/final/top_level_classes.png)

Tassonomia software:

![Software](doc/visualization/final/software.png)

Tassonomia sistemi operativi:
![Sistemi operativi](doc/visualization/final/operating_system.png)

Tassonomia licenze:

![Licenze](doc/visualization/final/license.png)

Tassonomia dipendenze:

![Dipendenze](doc/visualization/final/dependency.png)

### Template rappresentazione dati

Questo è una visualizzazione del template "standard" valido per la maggior parte dei software descritti:
![Software template](doc/visualization/final/software_template.png)

Qui si può vedere il template usato per descrivere l'applicativo __Xournal++__:
![Xournal](doc/visualization/final/xournalpp.png)

Riportiamo anche la visualizzazione dell'applicativo __Protégé__ per evidenziare un esempio di software astratto come la __Java Virtual Machine__:
![Protégé](doc/visualization/final/protege.png)

### Triple dell'esempio

| Subject      | Predicate | Object  |
| ----------- | ----------- |----------- |
| Xournalpp | rdf:type | owl:NamedIndividual |
| Xournalpp | rdf:type | ImplementedSoftware |
| Xournalpp | hasDependency | XournalppGTKDependency |
| Xournalpp | hasDependency | XournalppGlibcDependency |
| Xournalpp | hasDependency | XournalppLuaDependency |
| Xournalpp | hasDependency | XournalppZlibDependency |
| Xournalpp | isDistributedBy | Arch_Linux |
| Xournalpp | isDistributedBy | Ubuntu |
| Xournalpp | isDistributedBy | Debian |
| Xournalpp | isReleasedUnder | <http://spdx.org/licenses/GPL-2.0> |
| Xournalpp | isSupportedBy | Android |
| Xournalpp | isSupportedBy | Windows_10 |
| Xournalpp | isSupportedBy | macOS |
| Xournalpp | packagedBy | Flatpak |
| Xournalpp | packagedBy | Snap |
| Xournalpp | projectImageList | XournalppImageList |
| Xournalpp | provideFeature | GUI_interface |
| Xournalpp | provideFeature | Handwriting |
| Xournalpp | provideFeature | LatexSupport |
| Xournalpp | provideFeature | NoteTaking |
| Xournalpp | provideFeature | PDF_Annotation |
| Xournalpp | provideFeature | PDF_Export |
| Xournalpp | provideFeature | PluginSupport |
| Xournalpp | initialRelease | "2013-10-12T00:00:00"^^xsd:dateTime |
| Xournalpp | projectDescription | "Xournal++ is an [...]"@en |
| Xournalpp | projectHomepage | "https://xournalpp.github.io/" |
| Xournalpp | projectName | "Xournal++"@en |
| Xournalpp | projectRepository | "https://github.com/xournalpp/xournalpp/" |
| Xournalpp | projectWiki | "https://github.com/xournalpp/xournalpp/wiki" |
| Xournalpp | rdfs:comment | "Xournal++ is a hand note taking software [...]"@en |
| XournalppGTKDependency | rdf:type | owl:NamedIndividual |
| XournalppGTKDependency | rdf:type | Dependency |
| XournalppGTKDependency | dependencyTarget | GTK |
| XournalppGTKDependency | buildtimeDependency | "false"^^xsd:boolean |
| XournalppGTKDependency | isRequired | "true"^^xsd:boolean |
| XournalppGTKDependency | minVersion | 3 |
| XournalppGTKDependency | runtimeDependency | "true"^^xsd:boolean |
| XournalppGTKDependency | rdfs:comment | "Dependency from Xournal++ to GTK."@en |
| XournalppGTKDependency | rdfs:label | "XournalppGTKDependency"@en |
| XournalppGlibcDependency | rdf:type | owl:NamedIndividual |
| XournalppGlibcDependency | rdf:type | Dependency |
| XournalppGlibcDependency | dependencyTarget | glibc |
| XournalppGlibcDependency | buildtimeDependency | "false"^^xsd:boolean |
| XournalppGlibcDependency | isRequired | "true"^^xsd:boolean |
| XournalppGlibcDependency | runtimeDependency | "true"^^xsd:boolean |
| XournalppGlibcDependency | rdfs:comment | "Dependency from Xournal++ to glibc."@en |
| XournalppGlibcDependency | rdfs:label | "XournalppGlibcDependency"@en |
| XournalppImage1 | rdf:type | owl:NamedIndividual |
| XournalppImage1 | rdf:type | Image |
| XournalppImage1 | imageURI | "https://raw.githubusercontent.com/.../com.github.xournalpp.xournalpp.png"^^xsd:anyURI |
| XournalppImage1 | rdfs:comment | "Xournal++ icon"@en |
| XournalppImage1 | rdfs:label | "XournalppImage1"@en |
| XournalppImage2 | rdf:type | owl:NamedIndividual |
| XournalppImage2 | rdf:type | Image |
| XournalppImage2 | imageURI | "https://raw.githubusercontent.com/xournalpp/xournalpp/master/readme/main.png"^^xsd:anyURI |
| XournalppImage2 | rdfs:comment | "Screenshot on Linux"@en |
| XournalppImage2 | rdfs:label | "XournalppImage2"@en |
| XournalppImage3 | rdf:type | owl:NamedIndividual |
| XournalppImage3 | rdf:type | Image |
| XournalppImage3 | imageURI | "https://raw.githubusercontent.com/xournalpp/xournalpp/master/readme/main-win.png"^^xsd:anyURI |
| XournalppImage3 | rdfs:comment | "Screenshot on Windows 10"@en |
| XournalppImage3 | rdfs:label | "XournalppImage3"@en |
| XournalppImageList | rdf:type | owl:NamedIndividual |
| XournalppImageList | rdf:type | <http://www.ontologydesignpatterns.org/cp/owl/list.owl#List> |
| XournalppImageList| <http://www.ontologydesignpatterns.org/cp/owl/list.owl#hasFirstItem> | XournalppListItem1 |
| XournalppImageList| <http://www.ontologydesignpatterns.org/cp/owl/list.owl#hasLastItem> | XournalppListItem3 |
| XournalppImageList| <http://www.ontologydesignpatterns.org/cp/owl/bag.owl#size> | 3 |
| XournalppImageList| rdfs:comment | "A list of Xournal++ images|"@en |
| XournalppImageList| rdfs:label | "XournalppImageList"@en |
| XournalppListItem1 | rdf:type | owl:NamedIndividual |
| XournalppListItem1 | rdf:type |<http://www.ontologydesignpatterns.org/cp/owl/list.owl#ListItem> |
| XournalppListItem1 | <http://www.ontologydesignpatterns.org/cp/owl/bag.owl#itemContent> | XournalppImage1 |
| XournalppListItem1 | <http://www.ontologydesignpatterns.org/cp/owl/list.owl#nextItem> | XournalppListItem2 |
| XournalppListItem1 | rdfs:comment | "First list item containing a Xournal++ image."@en |
| XournalppListItem1 | rdfs:label | "XournalppListItem1"@en |
| XournalppListItem2 | rdf:type | owl:NamedIndividual |
| XournalppListItem2 | rdf:type | <http://www.ontologydesignpatterns.org/cp/owl/list.owl#ListItem> |
| XournalppListItem2 | <http://www.ontologydesignpatterns.org/cp/owl/bag.owl#itemContent> | XournalppImage2 |
| XournalppListItem2 | <http://www.ontologydesignpatterns.org/cp/owl/list.owl#nextItem> | XournalppListItem3 |
| XournalppListItem2 | rdfs:comment | "Second list item containing a Xournal++ image."@en |
| XournalppListItem2 | rdfs:label | "XournalppListItem2"@en |
| XournalppListItem3 | rdf:type | owl:NamedIndividual |
| XournalppListItem3 | rdf:type | <http://www.ontologydesignpatterns.org/cp/owl/list.owl#ListItem> |
| XournalppListItem3 | <http://www.ontologydesignpatterns.org/cp/owl/bag.owl#itemContent> | XournalppImage3 |
| XournalppListItem3 | rdfs:comment | "Third list item containing a Xournal++ image."@en |
| XournalppListItem3 | rdfs:label | "XournalppListItem3"@en |
| XournalppLuaDependency | rdf:type | owl:NamedIndividual |
| XournalppLuaDependency | rdf:type | Dependency |
| XournalppLuaDependency | dependencyTarget | lua |
| XournalppLuaDependency | buildtimeDependency | "false"^^xsd:boolean |
| XournalppLuaDependency | isRequired | "true"^^xsd:boolean |
| XournalppLuaDependency | runtimeDependency | "true"^^xsd:boolean |
| XournalppLuaDependency | rdfs:comment | "Dependency from Xournal++ to Lua."@en |
| XournalppLuaDependency | rdfs:label | "XournalppLuaDependency"@en |
| XournalppZlibDependency | rdf:type | owl:NamedIndividual |
| XournalppZlibDependency | rdf:type | Dependency |
| XournalppZlibDependency | dependencyTarget  | zlib |
| XournalppZlibDependency | buildtimeDependency | "false"^^xsd:boolean |
| XournalppZlibDependency | isRequired | "true"^^xsd:boolean |
| XournalppZlibDependency | runtimeDependency | "true"^^xsd:boolean |
| XournalppZlibDependency | rdfs:comment | "Dependency from Xournal++ to Zlib."@en |
| XournalppZlibDependency | rdfs:label | "XournalppZlibDependency"@en |

## SPARQL
### Flow chart interazione
```plantuml
@startuml
hide empty description
skinparam backgroundColor transparent
skinparam shadowing false
skinparam <<macro_state>> {
  padding 10
  StateFontSize 20
  StateBackgroundColor APPLICATION
}

state Search <<macro_state>>{
  state x <<choice>>
  x --> FeatureFilter: List Feature filters
  x --> OS_Filter: List OS filters
  x --> LicenseFilter: List License filters
  x --> TextFilter: Input text string
  FeatureFilter --> UpdateSearch: Add filter
  OS_Filter --> UpdateSearch: Add filter
  LicenseFilter --> UpdateSearch: Add filter
  TextFilter --> UpdateSearch: Add filter
}
state Software <<macro_state>>{
  state y <<choice>>
  y --> License
  y --> Author
  y --> OS
  y --> ProgrammingLanguage
  y --> Dependency
}
state ExternalResources <<macro_state>>{
  state Wikidata
  state SPDX
  state OS_homepage
}

[*] --> x: Open website
UpdateSearch --> y: Click on software
Dependency -up-> TargetSoftware
TargetSoftware -up-> Software
License --> SPDX
Author --> Wikidata
OS --> OS_homepage
ProgrammingLanguage --> Wikidata
@enduml

```

## Schema di interfaccia
### Homepage
![Prima_schermata](./doc/mockups/1.svg)
### Risultati ricerca
![Seconda_schermata](./doc/mockups/2.svg)
### Pagina di progetto
![Terza_schermata](./doc/mockups/3.svg)

## Esempio di interazione con dati reali
### Caso d'uso
L'utente vuole cercare tutti i software che rispettino i seguenti vincoli:
1. Software supportati dal sistema operativo "Debian GNU/Linux"
2. Software che forniscono la feature di "Text editing"
3. Software che forniscono la feature di "Spellchecking"

### Ipotetico flusso di interazione dell'utente
1. Selezione dei vincoli sopra definiti
2. Sottomissione della query di ricerca
3. Selezione ed espansione di una _pagina di progetto_ tra i risultati pervenuti. (Si supponga l'utente scelga l'applicativo "Vim")
4. Presa visione generale delle informazioni nella _pagina di progetto_ di "Vim"
5. Verifica di soddisfacimento dei vincoli di dipendenza di "Vim" nel proprio sistema operativo
6. Visita dell'homepage del progetto per procedere alla sua installazione

## SPARQL Queries
### 1. All OSs
#### Query
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?os ?os_label
WHERE {
    ?os a/rdfs:subClassOf* :Operating_System.
    ?os rdfs:label ?os_label
 }
```
#### Descrizione
Questa query permette di ottenere tutti i sistemi operativi descritti nel knowledge graph.  
I risultati ottenuti vengono utilizzati per definire i filtri in fase di ricerca dei progetti software.
#### Risultato
|os                                |os_label        |
|----------------------------------|----------------|
|http://purl.org/osso#Android      |Android         |
|http://purl.org/osso#Arch_Linux   |Arch Linux      |
|http://purl.org/osso#FreeBSD_OS   |FreeBSD OS      |
|http://purl.org/osso#NetBSD       |NetBSD          |
|http://purl.org/osso#OpenBSD_OS   |OpenBSD OS      |
|http://purl.org/osso#Ubuntu       |Ubuntu          |
|http://purl.org/osso#Windows_10   |Windows 10      |
|http://purl.org/osso#Windows_Phone|Windows Phone   |
|http://purl.org/osso#iPhone_OS    |iPhone OS       |
|http://purl.org/osso#macOS        |macOS           |
|http://purl.org/osso#Debian       |Debian GNU/Linux|

### 2. All features
#### Query
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?feature ?feature_name
WHERE {
    ?feature a/rdfs:subClassOf* :Feature.
    ?feature :featureName ?feature_name
 }
```
#### Descrizione
Questa query permette di ottenere tutte le feature descritte nel knowledge graph.  
I risultati ottenuti vengono utilizzati per definire i filtri in fase di ricerca dei progetti software.
#### Risultato
|feature                           |feature_name    |
|----------------------------------|----------------|
|http://purl.org/osso#Audio_decoding|Audio decoding  |
|http://purl.org/osso#Audio_encoding|Audio encoding  |
|http://purl.org/osso#AutoSaving   |Auto-Saving     |
|http://purl.org/osso#BackgroundPlaying|Background Playing|
|http://purl.org/osso#CLI_interface|CLI interface   |
|http://purl.org/osso#CalendarIntegration|Calendar integration|
|http://purl.org/osso#Data_compression|Data compression|
|http://purl.org/osso#Data_cryptography|Data cryptography|
|http://purl.org/osso#GUI_interface|GUI interface   |
|http://purl.org/osso#Handwriting  |Handwriting     |
|http://purl.org/osso#Journal      |Journal         |
|http://purl.org/osso#LatexSupport |Latex Support   |
|http://purl.org/osso#Lightweight  |Lightweight     |
|http://purl.org/osso#Modal_editing|Modal editing   |
|http://purl.org/osso#NoteTaking   |Note taking     |
|http://purl.org/osso#OWL_Editing  |OWL Editing     |
|http://purl.org/osso#PDF_Annotation|PDF Annotation  |
|http://purl.org/osso#PDF_Export   |PDF Export      |
|http://purl.org/osso#PIP          |Picture-in-Picture|
|http://purl.org/osso#PluginSupport|Plug-ins support|
|http://purl.org/osso#Programming_language_interpreter|Programming language interpreter|
|http://purl.org/osso#ReasoningSupport|Reasoning support|
|http://purl.org/osso#Spellchecking|Spellchecking   |
|http://purl.org/osso#Syntax_highlighting|Syntax highlighting|
|http://purl.org/osso#Text_editing |Text editing    |
|http://purl.org/osso#Video_decoding|Video decoding  |
|http://purl.org/osso#Video_encoding|Video encoding  |
|http://purl.org/osso#Virtual_Machine|Virtual Machine |
|http://purl.org/osso#Web_navigating|Web navigation  |
|http://purl.org/osso#Webpage_rendering|Webpage rendering|
|http://purl.org/osso#packages_distribution|Packages distribution|

### 3. Searching softwares
#### Query
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?project_name ?project
WHERE {
    ?project :projectName ?project_name.
    FILTER regex(?project_name, "search_text", "i").
    ?project :provideFeature :Text_editing. # for all feature filters
    ?project (:isSupportedBy | :isDistributedBy) :Ubuntu. # for all OS filters
    ?project :releasedUnder/a/rdfs:subClassOf* :FreeLicense. # for each license(class!!) filter
 }
```
#### Descrizione
Questa query effettua una ricerca dei software componendo tra loro quattro tipologie di filtri:  
1. Contenuto di una chiave di ricerca nel nome del progetto.
2. Fornitura di una feature da parte del progetto.
3. Supporto del progetto su un determinato sistema operativo.
4. Rilascio del progetto sotto una definita **classe** di licenze software.

La stessa tipologia di filtro può occorrere più volte e tutti i filtri sono in congiunzione tra loro.  
I risultati ottenuti, conformi con i vincoli precomposti, permettono all'utente di visualizzare i software risultanti e di espanderli nelle loro _pagine di progetto_.
#### Risultato
Risultati conformi all'esempio di interazione con dati reali.
|project_name                      |project         |
|----------------------------------|----------------|
|GNU Nano                          |http://purl.org/osso#GNU_nano|
|Vim                               |http://purl.org/osso#Vim|

### 4. Software main information
#### Query
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT ?project_name ?project_desc ?gallery_list ?initialRelease ?homepage ?repository ?wiki ?forum
WHERE {
  :project
        :projectName ?project_name;
        :projectDescription ?project_desc.
    OPTIONAL { :project :projectImageList ?gallery_list}.
    OPTIONAL { :project :initialRelease ?initialRelease}.
    OPTIONAL { :project :projectHomepage ?homepage}.
    OPTIONAL { :project :projectRepository ?repository}.
    OPTIONAL { :project :projectWiki ?wiki}.
    OPTIONAL { :project :projectForum ?forum}.
}
```
#### Descrizione
Questa query partendo da uno specifico progetto ne restituisce le principali informazioni ad esso associate.  
In particolare vengono restituite le seguenti informazioni:
- Nome del progetto.
- Descrizione del progetto.
- Se definita, la _lista_ contenente le immagini del progetto.
- Se definita, la data di primo rilascio.
- Se definito, il riferimento alll'homepage del progetto.
- Se definito, il riferimento alla repository della _codebase_ del progetto.
- Se definito, il riferimento alla wiki del progetto.
- Se definito, il riferimento al forum del progetto.

Il risultato viene utilizzato per definire le principali informazioni di un progetto all'intenterno di una _pagina di progetto_.
#### Risultato
Risultato conforme all'esempio di interazione con dati reali.
|project_name|project_desc                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |gallery_list                     |initialRelease     |homepage            |repository                |wiki|forum|
|------------|---------------------------------|---------------------------------|-------------------|--------------------|--------------------------|----|-----|
|Vim         |Vim is an advanced text editor...|http://purl.org/osso#VimImageList|1991-11-02T00:00:00|https://www.vim.org/|https://github.com/vim/vim|    |     |

### 5. Project features
#### Queries
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT ?feature_name ?feature_desc
WHERE {
   :project :provideFeature ?feature.
    ?feature :featureName ?feature_name.
    OPTIONAL { ?feature :featureDescription ?feature_desc}.
 }
```

#### Descrizione
Dato un progetto questa query restituisce tutte le feature che esso fornisce e, se disponibili, le loro descrizioni.  
La query viene usata per visualizzare le feature di un progetto all'interno di una _pagina di progetto_.

#### Risultato
Risultato conforme all'esempio di interazione con dati reali.
|feature_name|feature_desc                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
|------------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
|CLI interface|A command-line interface (CLI) processes commands to a computer program in the form of lines of text.                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
|Lightweight |A lightweight software also called lightweight program and lightweight application, is a computer program that is designed to have a small memory footprint (RAM usage) and low CPU usage, overall a low usage of system resources.                                                                                                                                                                                                                                                                                                                         |
|Modal editing|A modal text editor offers multiple interaction modes which are optimized for specific types of action and interaction. The rationale is that each mode is a finely-tuned tool which allows the user to realize their objectives in an efficient and powerful manner. This does require more learning than you might be accustomed to as a user of a non-modal editor, but the reward is hopefully increased efficiency and power.                                                                                                                          |
|Plug-ins support|Software's functionalities can be extended through different plug-ins.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
|Spellchecking|In software, a spell checker (or spell check) is a software feature that checks for misspellings in a text. Spell-checking features are often embedded in software or services, such as a word processor, email client, electronic dictionary, or search engine.                                                                                                                                                                                                                                                                                            |
|Syntax highlighting|This feature displays text, especially source code, in different colours and fonts according to the category of terms. This feature facilitates writing in a structured language such as a programming language or a markup language as both structures and syntax errors are visually distinct.                                                                                                                                                                                                                                                            |
|Text editing|A software able to edit plain text.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         |

### 6. Project licenses
#### Query
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX spdx: <http://spdx.org/rdf/terms#>

SELECT ?license ?name
FROM <http://spdx.org/rdf/terms#>
WHERE {
    :project :isReleasedUnder/owl:sameAs? ?license.
    ?license spdx:name ?name
 }
```

#### Descrizione
Dato un progetto questa query fornisce tutte le licenze con cui esso viene rilasciato.  
La query viene usata per visualizzare le licenze di un progetto all'interno di una *pagina di progetto*.

#### Risultato
Risultato conforme all'esempio di interazione con dati reali.
|license|name                              |
|-------|----------------------------------|
|http://spdx.org/licenses/Vim|Vim License  |

### 7. Project OSs
#### Query

```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT DISTINCT ?os_name ?os_desc ?homepage
WHERE {
    :project (:isSupportedBy | :isDistributedBy) ?os.
    ?os rdfs:label ?os_name.
    ?os rdfs:comment ?os_desc.
    OPTIONAL {?os foaf:homepage ?homepage}.
 }

```

#### Descrizione

Dato un progetto questa query fornisce tutti i sistemi operativi che lo supportano.   
Le informazioni fornite riguardano il nome, la descrizione e, se presente, l'homepage dei sistemi operativi risultanti.   
La query viene usata per visualizzare i sistemi operativi che supportano il dato progetto all'interno della sua *pagina di progetto*.

#### Risultato

Risultato conforme all'esempio di interazione con dati reali.
|os_name|os_desc                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     |homepage                    |
|-------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|----------------------------|
|Arch Linux|Arch Linux is a Linux distribution and is focused on simplicity, modernity, pragmatism, user centrality, and versatility.                                                                                                                                                                                                                                                                                                                                                                                                                                   |https://archlinux.org/      |
|FreeBSD OS|FreeBSD is a free and open-source Unix-like operating system descended from the Berkeley Software Distribution (BSD).                                                                                                                                                                                                                                                                                                                                                                                                                                       |https://www.freebsd.org/    |
|NetBSD |NetBSD is a free and open-source Unix-like operating system based on the Berkeley Software Distribution (BSD).                                                                                                                                                                                                                                                                                                                                                                                                                                              |https://www.netbsd.org/     |
|OpenBSD OS|OpenBSD is a security-focused, free and open-source, Unix-like operating system based on the Berkeley Software Distribution (BSD).                                                                                                                                                                                                                                                                                                                                                                                                                          |https://www.openbsd.org/    |
|Ubuntu |Ubuntu is a Linux distribution based on Debian GNU/Linux and composed mostly of free and open-source software.                                                                                                                                                                                                                                                                                                                                                                                                                                              |https://ubuntu.com/         |
|Windows 10|Windows 10 is an operating systems developed by Microsoft and released as part of its Windows NT family of operating systems.                                                                                                                                                                                                                                                                                                                                                                                                                               |https://www.microsoft.com/  |
|macOS  |macOS is a desktop operating system created and developed by Apple Inc. exclusively for its hardware.                                                                                                                                                                                                                                                                                                                                                                                                                                                       |https://www.apple.com/macos/|
|Debian GNU/Linux|Debian GNU/Linux is a Linux distribution composed of free and open-source software, developed by the community-supported Debian Project.                                                                                                                                                                                                                                                                                                                                                                                                                    |https://www.debian.org/     |

### 8. Project gallery images
#### Query

```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX c_entity: <http://www.ontologydesignpatterns.org/cp/owl/collectionentity.owl#>
PREFIX bag: <http://www.ontologydesignpatterns.org/cp/owl/bag.owl#>

SELECT ?imageIRI
WHERE {
    :project :projectImageList/c_entity:hasMember/bag:itemContent/:imageURI ?imageIRI
 }
```

#### Descrizione

Dato un progetto questa query fornisce, se presenti, tutte le immagini che compongono la galleria dello stesso.  
La query viene usata per visualizzare le immagini della galleria di un dato progetto nella sua *pagina di progetto*.

#### Risultato

Risultato conforme all'esempio di interazione con dati reali.
|imageIRI                                                                          |
|----------------------------------------------------------------------------------|
|https://upload.wikimedia.org/wikipedia/commons/8/8c/Vim-%28logiciel%29-console.png|

### 9. Similar suggested projects
#### Query
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT ?p (COUNT(?p) AS ?similarity)
WHERE {
    {SELECT DISTINCT ?p WHERE {?p a/rdfs:subClassOf* :Software. FILTER (?p != :project)}}
    ?p :provideFeature/^:provideFeature :project.
 }
GROUP BY ?p
HAVING (COUNT(?p) >= [MIN_IN_COMMON_FEATURES])
ORDER BY DESC (?similarity)
LIMIT [MAX_SIMILAR_APP]

```

#### Descrizione
Dato un progetto questa query fornisce, in ordine di similarità, i progetti simili in termini features in comune.  
Viene inoltre definito il **minimo numero di feature in comune** e il **massimo numero di suggerimenti** restituibili.  
All'interno di una *pagina di progetto* la query viene usata per suggerire all'utente un insieme di progetti simili e correlati.

#### Risultato
Risultato conforme all'esempio di interazione con dati reali.
|p                            |similarity|
|-----------------------------|----------|
|http://purl.org/osso#GNU_nano|5         |

### 10. Project dependencies
#### Query

```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>

SELECT ?dep_target ?minVersion ?exactVersion ?required ?runtime ?buildtime ?isAbstract ?isAlternative
WHERE {
    :project :hasDependency ?dep.
    ?dep :dependencyTarget ?dep_target.
    OPTIONAL {?dep :minVersion ?minVersion}.
    OPTIONAL {?dep :exactVersion ?exactVersion}.
    OPTIONAL {?dep :isRequired ?required}.
    OPTIONAL {?dep :runtimeDependency ?runtime}.
    OPTIONAL {?dep :buildtimeDependency ?buildtime}.
    BIND (EXISTS{?dep_target a :AbstractSoftware} AS ?isAbstract).
    BIND (EXISTS{?dep :alternativeDependency []} AS ?isAlternative)
 }
```

#### Descrizione

Dato un progetto questa query fornisce tutte le dipendenze verso altri progetti software.  
Di ogni dipendenza vengono estratte le seguenti informazioni:  
1. Il progetto _target_ di quella dipendenza.
2. Se definita, la minima versione che il target software **DEVE** soddisfare.
3. Se definita, l'esatta versione che il target software **DEVE** soddisfare.
4. Se definita, l'opzionalità di tale dipendenza.
5. Se definita, la necessità di soddisfacimento di tale dipendenza a _tempo di esecuzione_.
6. Se definita, la necessità di soddisfacimento di tale dipendenza a _tempo di compilazione_.
7. Se tale dipendenza è astratta o definita.
8. Se tale dipendenza è mutualmente esclusiva verso altre dipendenze.

La query viene usata per visualizzare le dipendenze che devono essere soddisfatte dal dato progetto.

#### Risultato
Risultato conforme all'esempio di interazione con dati reali.
|dep_target                    |minVersion|exactVersion|required|runtime|buildtime|isAbstract|isAlternative|
|------------------------------|----------|------------|--------|-------|---------|----------|-------------|
|http://purl.org/osso#glibc    |          |            |1       |1      |1        |0         |0            |
|http://purl.org/osso#libgcrypt|          |            |1       |1      |1        |0         |0            |
|http://purl.org/osso#lua      |          |            |0       |1      |0        |0         |0            |
|http://purl.org/osso#zlib     |          |            |1       |1      |1        |0         |0            |


### 11. Authors and developers of a project (Federated query)
#### Query
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>


SELECT DISTINCT ?author SAMPLE(?author_label) as ?label
WHERE {
    :project skos:relatedMatch ?wd_item
    SERVICE <https://query.wikidata.org/bigdata/namespace/wdq/sparql>
        {
            ?wd_item (wdt:P170|wdt:P178) ?author. # P170: creator; P178: developer;
            ?author rdfs:label ?author_label.
            filter langMatches(lang(?author_label), "en") }
}
GROUP BY ?author
```

#### Descrizione
Dato un progetto questa query restituisce gli autori e gli sviluppatori sulla base dell'allineamento con il knowledge graph di [WikiData](https://www.wikidata.org/).  
La query viene usata per visualizzare autori e sviluppatori all'interno di una _pagina di progetto_.

#### Risultato
Risultato conforme all'esempio di interazione con dati reali.
|author                               |label         |
|-------------------------------------|--------------|
|http://www.wikidata.org/entity/Q92885|Bram Moolenaar|

### 12. Programming languages used in a project (Federated query)
#### Query
```sparql
PREFIX : <http://purl.org/osso#>
PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>

SELECT DISTINCT ?language SAMPLE(?language_label) as ?label
WHERE {
    :project skos:relatedMatch ?wd_item
    SERVICE <https://query.wikidata.org/bigdata/namespace/wdq/sparql>
        {
            ?wd_item wdt:P277 ?language. # P277: programming language
            ?language rdfs:label ?language_label.
            filter langMatches(lang(?language_label), "en") }
}
GROUP BY ?language
```

#### Description
Dato un progetto questa query restituisce i linguaggi di programmazione utilizzati sulla base dell'allineamento con il knowledge graph di [WikiData](https://www.wikidata.org/).  
La query viene usata per visualizzare i linguaggi di programmazione usati in un progetto all'interno di una _pagina di progetto_.

#### Risultato
Risultato conforme all'esempio di interazione con dati reali.
|language                               |label     |
|---------------------------------------|----------|
|http://www.wikidata.org/entity/Q7931258|Vim script|
|http://www.wikidata.org/entity/Q15777  |C         |


## Breve documentazione dell'applicativo client
È stata realizzata una web application coerente con i requisiti definiti nei flussi di interazione di SPARQL.  
Per agevolare la manipolazione del DOM di html viene utilizzata la libreria open source jQuery.  
È stato inoltre utilizzato il framework Bootstrap 5, anchesso open source,  per gestire la presentazione e gli stili degli elementi HTML.  
La logica applicativa viene descritta tramite JavaScript e jQuery in appositi moduli dedicati alle varie pagine dell'applicativo web.  
Per quanto riguarda l'interrogazione dell'ontologia viene utilizzato Openlink Virtuoso interagendo con il suo SPARQL endpoint attraverso un modulo programmato ad-hoc.  

L'applicativo è composto da due schermate principali:
1. La pagina di ricerca dei progetti software, essa permette all'utente di selezionare i filtri di ricerca come descritto nelle precedenti sezioni e successivamente di vederne i risultati e navigarli.
1. La pagina di progetto, essa mostra all'utente tutti i dettagli di uno specifico progetto: nome, descrizione, progetti alternativi, features fornite, gallerie, licenze, dipendenze, sistemi operativi che lo supportano, autori, linguaggi di programmazione, homepage del progetto, repository della codebase, forum e wiki del progetto.

Le dipendenze di un software possono essere navigate dando all'utente una maggiore visione di insieme delle applicazioni, tecnologie e licenze software coinvolte.  
Vengono inoltre forniti i dettagli specifici delle varie dipendenze sia per la fase di compilazione del codice sorgente che per la sua esecuzione runtime.  
Viene interrogata la sorgente di dati esterna WikiData tramite due queries federate che integrano nella visualizzazione gli autori ed i linguaggi di programmazione coinvolti nel progetto.  
Le licenze, definite in Software Package Data Exchange (SPDX), possono essere aperte permettendo all'utente di prendere visione degli aspetti legali e giuridici delle stesse.  
Tutte le entità di WikiData, i link ai sistemi operativi e i link specifici del progetto(Homepage, repository, etc..) sono rese esplorabili e navigabili dall'utente.

## Regole SWRL

### Abstract Software's Features TO ImplementedSoftware

```
AbstractSoftware(?x) ^ provideFeature(?x, ?y) ^ implementedBy(?x, ?z) -> provideFeature(?z, ?y)
```

Questa regola SWRL assicura che dato un __software astratto__, per esempio la Java Virtual Machine, tutte le sue __implementazioni__ forniscono __almeno__ le sue __feature__.

### Alternative Dependencies referred from the same software

```
alternativeDependency(?x, ?y) ^ hasDependency(?s1, ?x) ^ hasDependency(?s2, ?y) -> sameAs(?s1, ?s2)
```

Questa regola assicura che due dipendenze se alternative tra loro appartengono allo stesso software.

### Consolidated Software

```
ImplementedSoftware(?s) ^ initialRelease(?s, ?init) ^
temporal:duration(?diff, ?init, "now", "Years") ^
swrlb:greaterThan(?diff, 10) -> ConsolidatedSoftware(?s)
```

Questa regola definisce come software consolidati tutti i software che sono stati rilasciati da almeno 10 anni. Per il calcolo della differenza temporale abbiamo usato l'operatore built-in **temporal:duration** e confrontato il valore con **swrlb:greaterThan**.

### New Software

```
ImplementedSoftware(?s) ^ initialRelease(?s, ?init) ^
temporal:duration(?diff, ?init, "now", "Years") ^
swrlb:lessThan(?diff, 1) -> NewSoftware(?s)
```

Questa regole definisce come nuovi software tutti quei software che sono stati rilasciati da meno di 1 anno.

Data la natura monotonica di SWRL le rimozione dei progetti software 'invecchiati' nel corso del tempo può essere eseguita utilizzando una query SPARQL 1.1 Update.

### Creative Commons

```
spdx:License(?x) ^ spdx:name(?x, ?y) ^ swrlb:contains(?y, "Creative Commons") -> CC_license(?x)
```

Questa regola assegna le licenze contenenti la stringa "Creative Commons" alla categoria di licenze corretta utilizzando l'operatore built-in __swrlb:contains__

